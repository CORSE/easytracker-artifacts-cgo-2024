#!/usr/bin/env bash
set -euo pipefail

dir="$(readlink -e "$(dirname "$0")")"

[ -d easytracker ] || { echo "ERROR: can't setup, easytracker directory does not exist" >&2; exit 1; }
[ ! -d easytracker/.venv ] || { echo "WARNING: skipping setup, already done (remove easytracker/.venv to restart)" >&2; exit 0; }

cleanup() {
    code=$?
    trap - EXIT INT TERM
    cd "$dir"
    [ $code = 0 ] || rm -rf easytracker/.venv
    exit $code
}

trap cleanup EXIT INT TERM

source "$dir"/PARAMS.sh

pushd easytracker >/dev/null

virtualenv -p python3 .venv
source .venv/bin/activate

make develop
pip install -r extra-packages/requirements.txt

popd >/dev/null
