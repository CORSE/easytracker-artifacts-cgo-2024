#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"
[ -d easytracker/.venv ] || { echo "ERROR: easytracker/.venv directory does not exists" >&2; exit 1; }

source "$dir"/PARAMS.sh

source easytracker/.venv/bin/activate
export PYTHONPATH="$PWD/easytracker/extra-packages"

TOOLSDIR=../easytracker/tools/
TOOLDIR=$TOOLSDIR/python/inv_visualizator
OUTDIR=figure-1

rm -rf "$OUTDIR"
mkdir "$OUTDIR"
pushd "$OUTDIR" >/dev/null

$TOOLDIR/inv_visualizator.py -li i -ri j $TOOLDIR/inferiors/sort_array_of_binaries.py sort array

popd >/dev/null

echo
echo "Graphs for figure 1 generated in figure-1/"
echo "One may open the actual figures used in figure 1 with:"
echo "  eog figure-1/source21.jpg &"
echo "  eog figure-1/array21.svg &"
echo
