#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"
[ -d easytracker/.venv ] || { echo "ERROR: easytracker/.venv directory does not exists" >&2; exit 1; }
[ -d OnlinePythonTutor ] || { echo "ERROR: OnlinePythonTutor directory does not exists" >&2; exit 1; }

source "$dir"/PARAMS.sh

source easytracker/.venv/bin/activate

OUTDIR=figure-10
rm -rf "$OUTDIR"
mkdir "$OUTDIR"

TOOLDIR="$dir"/paper-examples/python-tutor
cp "$TOOLDIR"/trace-ptutor.py "$TOOLDIR"/mystery.py "$OUTDIR"/
cp -pr OnlinePythonTutor/v3 "$OUTDIR"/ptv3

pushd "$OUTDIR" >/dev/null

./trace-ptutor.py mystery.py m x y > ptv3/test-trace.js

popd >/dev/null

echo
echo "Python tutor HTML page for figure 10 available in figure-10/ptv3/demo.html"
echo "One may open it in the browser and click 'Forward' until step 19:"
echo "  x-www-browser figure-10/ptv3/demo.html"
echo
