#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"
[ -d easytracker/.venv ] || { echo "ERROR: easytracker/.venv directory does not exists" >&2; exit 1; }

source "$dir"/PARAMS.sh

source easytracker/.venv/bin/activate
export PYTHONPATH="$PWD/easytracker/extra-packages"

TOOLSDIR=../easytracker/tools/
TOOLDIR=$TOOLSDIR/generic/stack_heap_visualizator
OUTDIR=figure-6

rm -rf "$OUTDIR"
mkdir "$OUTDIR"
pushd "$OUTDIR" >/dev/null

(cd $TOOLDIR/inferiors/c && make clean heap_pointer)

mkdir -p figure-6a figure-6b figure-6c
$TOOLDIR/stack_heap_visualizator.py $TOOLDIR/inferiors/python/stack_only.py -o figure-6a -so
$TOOLDIR/stack_heap_visualizator.py $TOOLDIR/inferiors/python/everything_ref_params_list.py -o figure-6b
$TOOLDIR/stack_heap_visualizator.py $TOOLDIR/inferiors/c/heap_pointer -o figure-6c

popd >/dev/null

echo
echo "Graphs for figure 6 generated in figure-6/figure6[abc]/*.svg"
echo "One may open the actual figures used in figure 6a, 6b, 6c with:"
echo "  eog figure-6/figure-6a/008-stack_heap.svg &"
echo "  eog figure-6/figure-6b/007-stack_heap.svg &"
echo "  eog figure-6/figure-6c/014-stack_heap.svg &"
echo
