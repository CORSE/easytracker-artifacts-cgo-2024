#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"

source "$dir"/PARAMS.sh

image="$DOCKER_IMAGE:$DOCKER_IMAGE_VERSION"
umask="$(umask)"
image_id="$(docker image ls -q "$image")"
if [ -z "$image_id" ]; then
    docker pull "$image" || { echo "ERROR: docker image can't be accessed through network, please upload the image from the artifacts and execute: docker load -i docker-image-easytracker-$DOCKER_IMAGE_VERSION.tar" >&2 ; exit 1; }
fi
exec docker run --rm -it -u $(id -u):$(id -g) -v $PWD:$PWD -w $PWD "$image" bash -c "umask $umask; $*"
