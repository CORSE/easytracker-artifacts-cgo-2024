#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"
[ ! -d OnlinePythonTutor ] || { echo "WARNING: skipping clone of OnlinePythonTutor, already done (remove OnlinePythonTutor/ to restart)" >&2; exit 0; }

source "$dir"/PARAMS.sh

PTDIR=OnlinePythonTutor

mkdir -p "$PTDIR"

pushd "$PTDIR" >/dev/null
git init
git remote add origin "$PT_URL"
git fetch origin --depth 1 "$PT_VERSION"
git reset --hard FETCH_HEAD

popd >/dev/null
