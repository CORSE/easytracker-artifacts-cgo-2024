#!/usr/bin/env python3

"""Visualize execution of recursive functions."""
import json
import sys

pt3_trace = []
def dump_trace(prog_name, outf=sys.stdout):
 with open(prog_name) as f: prog = f.read()
 pt3 = dict(code=prog, trace=pt3_trace)
 pt3_str = json.dumps(pt3)
 #outf.write(f'{pt3_str}')
 outf.write(f'var trace = {pt3_str};\n')

def record_call_entry(frame, func_name, args_names):
 record_frame(frame, func_name, args_names, frame.next_line, 'step_line')

def record_call_return(frame, func_name, args_names, retval):
 record_frame(frame, func_name, args_names, frame.last_line, 'return', retval)

def record_frame(frame, func_name, args_names, line, event, retval=None):
 from copy import deepcopy
 frame_defaults = { 'frame_id': None,
                    'encoded_locals': {},
                    'ordered_varnames': [],
                    'is_highlighted': False,
                    'is_parent': False,
                    'func_name': None,
                    'is_zombie': False,
                    'parent_frame_id_list': [],
                    'unique_hash': None}
 pt3_funcs = {}
 pt3_frames = []
 pt3_lists = {}
 first = True
 while frame != None:
  pt3_frame = deepcopy(frame_defaults)
  lvars = frame.variables
  if frame.name == func_name:
   lvars = {k: v for k, v in frame.variables.items() if k in args_names}
  pt3_lists.update({id(v.value): v.value for v in lvars.values() if
                    isinstance(v.value, list)})
  pt3_locals = {k: ['REF', id(v.value)] if isinstance(v.value, list) else v.value
                for k, v in lvars.items()}
  if first and event == 'return':
   pt3_locals['__return__'] = retval
  pt3_frame.update(dict(
   encoded_locals=pt3_locals,
   ordered_varnames=list(pt3_locals.keys()),
   func_name=frame.name,
   unique_hash=str(id(frame)),
  ))
  pt3_frames.append(pt3_frame)
  glob_name = f'{frame.name}({",".join(args_names)})' if frame.name == func_name else frame.name
  pt3_funcs[frame.name] = glob_name
  frame = frame.parent
  first = False
 pt3_frames = pt3_frames[::-1]
 for i, pt3_frame in enumerate(pt3_frames):
  pt3_frame['frame_id'] = i + 1
 if len(pt3_frames) > 0:
  pt3_frames[-1]['is_highlighted'] = True

 pt3_globals = {k: ['REF', id(k), None] for k, v in pt3_funcs.items()}
 pt3_heap = { str(id(k)): [ "FUNCTION", v, None]
              for k, v in pt3_funcs.items()}
 pt3_heap.update({k: ['LIST'] + v for k, v in pt3_lists.items()})

 pt3_step = dict(
  stdout='',
  func_name=func_name,
  stack_to_render=pt3_frames,
  ordered_globals=list(pt3_globals.keys()),
  globals=pt3_globals,
  heap=pt3_heap,
  line=line,
  event=event,
 )
 pt3_trace.append(pt3_step)

def control(prog_name, func_name, args_names):
 from types import SimpleNamespace as ns
 from easytracker.init_tracker import init_tracker
 from easytracker import PauseReasonType as prt

 tracker = init_tracker("python")
 tracker.load_program(prog_name)

 tracker.track_function(func_name)
 tracker.start()

 while tracker.exit_code is None:
  tracker.resume()
  reason = tracker.pause_reason
  if reason.type == prt.CALL:
   frame = tracker.get_current_frame(as_raw_python_objects=True)
   record_call_entry(frame, func_name, args_names)
  elif reason.type == prt.RETURN:
   frame = tracker.get_current_frame(as_raw_python_objects=True)
   record_call_return(frame, func_name, args_names, reason.args[2])

 tracker.terminate()
 dump_trace(prog_name)

import sys
argv = sys.argv
control(argv[1], argv[2], argv[3:])
