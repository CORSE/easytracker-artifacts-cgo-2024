#!/usr/bin/env python3
# pylint: disable=all


def m(x, y):
    z = x
    a = len(y) - 1 - x
    y[a] += 1
    if x == 0:
        return 1
    m1 = m(z - 1, y)
    m2 = m(x - 1, y)
    return m1 + m2


def main():
    r = 3
    b = [0] * (r + 1)
    m(r, b)


if __name__ == "__main__":
    main()
