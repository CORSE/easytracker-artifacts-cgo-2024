Generation of python-tutor traces
=================================

Example generating a trace usable by python tutor visualization.

First clone python tutor with the script and copy the `ptv3` subdir:

    ./clone-ptv3.sh

Then execute the contoller with the provided example program and copy the
trace in the python tutor v3 dir:

    ./trace-ptutor.py mystery.py m x y >test-trace.js
    cp test-trace.js OnlinePythonTutor/v3/

The generated trace can be opened with the browser:

    x-www-browser OnlinePythonTutor/v3/demo.html
