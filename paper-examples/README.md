Paper example tools, simplified for paper
=========================================

# Recursive tree visualization

Simplified version of recursive visualization, in `recvis/` directory

# Debugging game

Simplified version of the debugging game tool, in `interactive/` directory

# Python Tutor Trace

Generation of python tutor traces, in `python-tutor/` directory
