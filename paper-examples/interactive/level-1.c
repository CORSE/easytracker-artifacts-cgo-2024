/* level-1 */

#include <stdlib.h>
#include <stdio.h>

typedef enum { UP, DOWN, LEFT, RIGHT } orientation;

int player_x, player_y, exit_x, exit_y, key_x, key_y;
orientation player_o;
int has_key;

void msg(const char *msg) {
  puts(msg);
}

void check_key() {
  if (player_x == key_x && player_y == key_y) {
    msg("Found a key!");
    //has_key = 1; // Player should set somewhere has_key = 1
    key_x = -1;
    key_y = -1;
  }
}

void verify_exit() {
  if (player_x != exit_x || player_y != exit_y) {
    msg("Did not find the door!");
    exit (EXIT_FAILURE);
  } else if (has_key != 1) {
    msg("Unable to open the door!");
    exit (EXIT_FAILURE);
  } else {
    msg("Completed Level!");
  }
}

void turn_left () {
  static orientation new_o[] = {LEFT, RIGHT, DOWN, UP};
  player_o = new_o[player_o];
}

void turn_right () {
  static orientation new_o[] = {RIGHT, LEFT, UP, DOWN};
  player_o = new_o[player_o];
}

void forward() {
  int inc_x[] = {0, 0, -1, 1};
  int inc_y[] = {-1, 1, 0, 0};

  player_x += inc_x[player_o];
  player_y += inc_y[player_o];
  check_key();
}

void init_level() {
  player_o = RIGHT;
  has_key = -1;
  player_x = 3; player_y = 2;
  key_x = 5; key_y = 1;
  exit_x = 7; exit_y = 0;
}

int main() {
  char simu[] = "fflffrff"; // Simulated for the example
  char *input = simu;

  init_level();
  while (*input != '\0') {
    switch (*input) {
      case 'f': forward(); break;
      case 'l': turn_left(); break;
      case 'r': turn_right(); break;
    }
    input++;
  }
  verify_exit();
  return 0;
}
