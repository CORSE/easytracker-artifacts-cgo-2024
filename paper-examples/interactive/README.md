Simple implementation of Debugging Game
=======================================

Compact implementation used for papers.

Run interactively with:
    ./game.py

In another window open `level-1.c` and try to find the bug!

