#!/usr/bin/env python3


def build_level(fname):
 from subprocess import run
 p = run(f'gcc -g -O0 -o {fname} {fname}.c', shell=True)
 return p.returncode

def ask_continue(name):
 cmd = input(f'Please edit {name}.c then Enter or q: ')
 return cmd != "q"

def run_level_1():
 from easytracker import init_tracker
 from easytracker import PauseReasonType as prt

 name = 'level-1'
 exit_code = None
 tries, show_on_key, show_has_key = 0, False, False
 watch_vars = ["has_key", "key_x", "key_y",
      "player_x", "player_y"]

 while exit_code != 0:
  exit_code = build_level(name)
  if exit_code != 0:
   print('Program does not compile')
   if not ask_continue(name):
    break
   continue
  tracker = init_tracker("GDB")
  tracker.load_program(name)
  reason = tracker.start()
  for var in watch_vars:
   tracker.watch(var)
  check_bkp = tracker.break_before_func("check_key")
  cvars = {}
  while reason.type != prt.EXITED:
   if (show_on_key and
       reason.type == prt.BREAKPOINT and
       reason.args[0] == check_bkp and
       cvars['player_x'] == cvars['key_x'] and
       cvars['player_y'] == cvars['key_y']):
     print(f'The user moved over the key at '
           f'{cvars["key_x"]}, {cvars["key_y"]}')
   elif reason.type == prt.WATCHPOINT:
    cvars[reason.args[1]] = reason.args[3]
    if show_has_key and reason.args[1] == "has_key":
     print(f'The "has_key" value changed from '
           f'{reason.args[2]} to {reason.args[3]}')
   reason = tracker.resume()
  exit_code = tracker.exit_code
  if exit_code != 0:
   print(f'Bug still present!')
   if tries >= 1:
    print(f'Hint: "has_key" variable value is '
          f'{cvars["has_key"]}')
   if tries >= 2:
    print(f'Hint: you will be notified when '
          f'the player goes over the key')
    show_on_key = True
   if tries >= 3:
    print(f'Hint: you will be notified when '
          f'the "has_key" value changes')
    show_has_key = True
   if not ask_continue(name):
    tracker.terminate()
    break
   tries += 1
  else:
   print(f'Successfully fixed bug in {name}.c, '
         f'we get to the next level!')
  tracker.terminate()
 return exit_code

run_level_1()
