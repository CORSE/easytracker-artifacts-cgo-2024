Recursive visualization, simplified for paper
=============================================

The `recvis.py` script is a simplified version of `rec_visualizator.py` with
basically one `control` function and one `visualize` function outputing png files and
visualizing the files with `eog`.

Run with:

    ./recvis.py mystery.py m x elems   # python inferior

