#!/usr/bin/env python3

def m(x, elems):
    idx = len(elems) - 1 - x
    elems[idx] += 1
    if x == 0:
        return 1
    m1 = m(x - 1, elems)
    m2 = m(x - 1, elems)
    return m1 + m2

def main():
    r = 3
    elems = [0] * (r + 1)
    m(r, elems)
    print(elems)

if __name__ == "__main__":
    main()