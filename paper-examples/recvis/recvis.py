#!/usr/bin/env python3

"""Visualize execution of recursive functions."""

def visualize(name, call, func_name, next_lineno,
              last_lineno, prog_name):
 from subprocess import run
 from visualprimitives.source_image import (
  generate_source_image)

 with open(f'{name}.dot', "w") as f:
  f.write('digraph rec {\n')
  dump_call_tree(f, call, func_name)
  f.write('}\n')
 run(f'dot -Tsvg {name}.dot -o {name}.svg', shell=True)
 run(f'[ -z "$DISPLAY" ] || eog -w {name}.svg &', shell=True)
 generate_source_image(prog_name,f"{name}_src",
  next_lineno)

def dump_call_tree(dot_file, call, func_name):
 from easytracker import AbstractType as at
 def to_str(val):
  if val.abstract_type == at.PRIMITIVE:
   return str(val.content)
  elif val.abstract_type == at.LIST:
   return str([to_str(x) for x in val.content])
  elif val.abstract_type == at.REF:
   return to_str(val.content)
 args = ', '.join([to_str(arg) for arg in call.args])
 color = 'chartreuse' if call.active else 'lightgray'
 dot_file.write(
  f'{call.uid} '
  f'[color="{color}" style="filled"'
  f'shape="rect" margin="0" width="1"'
  f'label="{call.uid}.\\n{func_name}({args})"]\n')
 if call.parent:
  ecol = 'black' if call.active else 'lightgray'
  rcol = 'black' if call.parent.active else 'lightgray'
  rsty = 'style="invis"' if call.active else ''
  dot_file.write(
   f'{call.parent.uid} -> {call.uid} '
   f'[color="{ecol}"]\n')
  dot_file.write(
   f'{call.uid} -> {call.parent.uid} '
   f'[label="{call.retval}" '
   f'color="{rcol}" {rsty}]\n')
 for child in call.child:
  dump_call_tree(dot_file, child, func_name)

def control(prog_name, func_name, args_names):
 from types import SimpleNamespace as ns
 from easytracker.init_tracker import init_tracker
 from easytracker import PauseReasonType as prt
 from copy import deepcopy

 tracker = init_tracker(
  "python" if prog_name.endswith(".py") else "GDB")
 tracker.load_program(prog_name)

 current, calls, idx = None, [], 0
 tracker.track_function(func_name)
 tracker.start()

 while tracker.exit_code is None:
  tracker.resume()
  reason = tracker.pause_reason
  if reason.type == prt.CALL:
   args = [
    tracker.get_variable_value(arg).value
    for arg in args_names]
   current = ns(args=args, uid=len(calls),
       child=[], parent=current, active=True,
       retval=None)
   if current.parent is not None:
    current.parent.child.append(current)
   calls.append(current)
  elif reason.type == prt.RETURN:
   current.retval = reason.args[2]
   current.active = False
   current = current.parent
  else:
   continue
  idx += 1
  visualize(f"rec-{idx:03}", calls[0], func_name,
            tracker.next_lineno, tracker.last_lineno,
            prog_name)

 tracker.terminate()

import sys
argv = sys.argv
control(argv[1], argv[2], argv[3:])
