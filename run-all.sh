#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"

"$dir"/clone.sh
"$dir"/setup.sh
"$dir"/generate-figure-1.sh
"$dir"/generate-figure-6.sh
"$dir"/generate-figure-7.sh
"$dir"/generate-figure-8.sh
"$dir"/run-game-figure-9.sh

"$dir"/clone-ptv3.sh
"$dir"/generate-figure-10.sh


