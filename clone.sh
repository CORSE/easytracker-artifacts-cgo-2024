#!/usr/bin/env bash
set -euo pipefail

dir="$(readlink -e "$(dirname "$0")")"
[ ! -d easytracker ] || { echo "WARNING: skipping clone, already done (remove easytracker/ to restart)" >&2; exit 0; }

cleanup() {
    code=$?
    trap - EXIT INT TERM
    cd "$dir"
    [ $code = 0 ] || rm -rf easytracker.tmp
    exit $code
}

trap cleanup EXIT INT TERM

source "$dir"/PARAMS.sh


rm -rf easytracker.tmp
mkdir -p easytracker.tmp

pushd easytracker.tmp >/dev/null
git init
git remote add origin "$EASY_URL"
git fetch origin --depth 1 "$EASY_VERSION" || { echo "ERROR: unable to clone easytracker from network, please download eastracker archive from artifacts and do: tar xzf easytracker-archive-${EASY_VERSION::10}.tar.gz" >&2; exit 1; }
git reset --hard FETCH_HEAD
popd >/dev/null

mv easytracker.tmp easytracker

