#!/usr/bin/env bash
set -euo pipefail
dir="$(dirname "$0")"

exec "$dir"/paper-examples/python-tutor/clone-ptv3.sh
