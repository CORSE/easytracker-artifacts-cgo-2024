Artifacts generation
====================

This repository contains the scripts for reproducing the artifacts of
the CGO 2024 EasyTracker paper.

Actually the artifacts are mainly example of usage of the EasyTracker
library and thus contains generated visualization figures and listings
using the library.

To be noted, two figures from the paper are just provided as is:
- figure 7, the RiscV memory/registers view: this figure was contributed
  by a teacher using a RiscV toolchain which we could not rebuild;
- figure 9, the snapshot of the Debugging Game: this figure was provided
  by the game writers and we could not reproduce it without providing the
  full game sources which is not in a publishable state.
  We provide, though, in the paper a very simplified program giving an
  idea of the game play, and this artifact is provided.

Follow the steps below to construct the artifacts.

Note that for a first test it is recommended to run in docker, ref to section
"In Docker execution" below.

## Systems requirements

Recommended system: Linux Ubuntu 22.04 x86_64

Requirements:
- python version >= 3.9 (ref WARNING below)
- gdb version >= 11.2   (ref WARNING below)
- packages listed below

Installation of packages on Ubuntu 22 (or equivalent debian):

    sudo apt-get install -y build-essential gcc g++ gdb time file git curl wget
    sudo apt-get install -y python3 python3-pip python3-virtualenv
    sudo apt-get install -y fontconfig graphviz eog

WARNING: without the proper python (>= 3.9) or gdb version (>= 11.2) the tool
may fail unexpectedly with cryptic python exception messages. In case of python
exception during the execution, verify first these two mandatory versions.

## In Docker execution

Alternatively to installing the packages and requirements above, on can use the
`./in-docker.sh` script provided and prefix the `run-all.sh` command with it.

First ensure that docker is installed, any version should work:

    docker --version
    Docker version ....
    
Then, for instance, execute the full artifacts generation (see details below):

    ./in-docker.sh ./run-all.sh

This command should be equivalent to executing directly `./run-all.sh` on an
environment with the requirements above (though, the execution is done in a docker container
already prepared with the required dependencies).

Note that, with this `./in-docker.sh` script, if one wants to execute the artifacts generation
step by step, as described below, first execute `./in-docker.sh bash` and then execute
the steps from there.
For instance:

    ./in-docker.sh bash
    ...   # One can ignore messages and warnings there
    guest@...$ ./clone.sh
    guest@...$ ./setup.sh
    guest@...$ ./generate-figure-1.sh
    guest@...$ ...
    guest@...$ exit # leave the container


## Artifacts generation

Execute the `./run-all.sh` script in order to build all artifacts:

    ./run-all.sh

Alternatively (equivalent to ./run-all.sh) , execute the following scripts in order:

    ./clone.sh               # Clone easytracker in dir: easytracker/
    ./setup.sh               # Setup/install easytracker in a venv: easytracker/.venv
    ./generate-figure-1.sh   # Generate images for figure 1 in: figure-1/*.jpg
    ./generate-figure-6.sh   # Generate images for figure 6 in: figure-6/figure-6[abc]/*.svg
    ./generate-figure-7.sh   # Get image for figure 7 in: figure-7/*.png
    ./generate-figure-8.sh   # Generate images for figure 8 in: figure-8/*.svg
    ./run-game-figure-9.sh   # Run non-interactively the simplified version of the debugging game

    ./clone-ptv3.sh          # Clone Python Tutor Visualizer in dir: OnlinePythonTutor/
    ./generate-figure-10.sh  # Generate HTML page to open in: figure-10/ptv3/demo.html


If one wants to execute the tools directly without these wrapper scripts,
once the `./setup.sh` script has been run, setup the environment in the current shell with:

    source easytracker/.venv/bin/activate
    export PYTHONPATH $PWD/easytracker/extra-packages


## Artifacts Images and Listings

After execution of the `run-all.sh` script, the following images
are available for the different figures:

- Figure 1: array invariant visualization
  - source: `figure-1/source21.jpg`
  - array: `figure-1/array21.svg`
- Figure 6: stack and heap visualization
  - 6a: `figure-6/figure-6a/008-stack_heap.svg`
  - 6b: `figure-6/figure-6b/007-stack_heap.svg`
  - 6c: `figure-6/figure-6c/014-stack_heap.svg`
- Figure 7: RiscV registers and memory viewer (NOTE: these figures are provided as is)
  - source: `figure-7/riscv_src.jpg`
  - memory and registers: `figure-7/riscv_viewer.png`
- Figure 8: recursive function call tree visualization
  - source: `figure-8/rec-013_src.jpg`
  - call tree graph: `figure-8/rec-013.svg`
- Figure 9: debugging game
  - listings: see below in Listings section
  - game snapshot: `figure-9/game-display.png` (NOTE: the image is provided as is)
  - execute non interactively the simplified version of the paper with: `./run-game-figure-9.sh`
  - or interactively by going in: `paper-examples/interactive`,
    set the environment as explained above and ref to the REAME there
- Figure 10: python tutor visualization
  - generated HTML: `figure-10/ptv3/demo.html`
  - actually this is the HTML page of the Python Tutor Visualizer that opens the
    generated trace for figure 10
  - open it with `x-www-browser figure-10/ptv3/demo.html` and click "Forward" button
    until step 19 to see the figure 10 of the paper

Listings given in paper are available in:

- Listing 6: recursive tree visualization:
  - recvis `control` and `visualize` code in `paper-examples/recvis/recvis.py`
- Listing of Figure 9: debugging game:
  - controller sources in `paper-examples/interative/game.py`
  - code to debug (the C code of a game level) in `paper-examples/interative/level-1.c`
