EASY_URL="https://gitlab.inria.fr/CORSE/easytracker.git"
EASY_VERSION=dfe8aa888f5edfcd176124f7cd03a1e768674d91

DOCKER_IMAGE="registry.gitlab.inria.fr/corse/easytracker/easytracker"
DOCKER_IMAGE_VERSION="1.2.0"
