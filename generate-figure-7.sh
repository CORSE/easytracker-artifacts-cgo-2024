#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"
[ -d easytracker/.venv ] || { echo "ERROR: easytracker/.venv directory does not exists" >&2; exit 1; }

source "$dir"/PARAMS.sh

source easytracker/.venv/bin/activate
export PYTHONPATH="$PWD/easytracker/extra-packages"

OUTDIR=figure-7

rm -rf "$OUTDIR"
mkdir "$OUTDIR"

echo "NOTE: automatic generation of riscv mem/regs image for figure 7 not provided, using image snapshot as is"
cp snapshots/riscv_src.jpg snapshots/riscv_viewer.png "$OUTDIR"

echo
echo "Image for figure 7 in figure-7/"
echo "One may find the images of figure 7 in (NOTE: these images are not generated, but provided as is):"
echo "  eog figure-7/riscv_src.jpg &"
echo "  eog figure-7/riscv_viewer.png &"
echo

