#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"
[ -d easytracker/.venv ] || { echo "ERROR: easytracker/.venv directory does not exists" >&2; exit 1; }

source "$dir"/PARAMS.sh

source easytracker/.venv/bin/activate

OUTDIR=figure-9
rm -rf "$OUTDIR"
mkdir "$OUTDIR"

TOOLDIR="$dir"/paper-examples/interactive
cp "$TOOLDIR"/game.py "$TOOLDIR"/level-1.c "$OUTDIR"/

echo "NOTE: automatic generation of gameplay image for figure 9 not provided, using image snapshot as is"
cp "$dir"/snapshots/game-display.png "$OUTDIR"/

pushd "$OUTDIR" >/dev/null

# Running game without modifying level-1.c, gives incremental hints
echo $'\n\n\n\nq' | ./game.py

# Fix bug in level-1.c and relaunch
sed -i 's/^.*has_key = 1.*/has_key = 1;/' level-1.c
echo | ./game.py

popd >/dev/null

echo
echo "Game was run and level-1.c corrected"
echo "One may find the fixed level version and the controller code of figure 9 in:"
echo "  figure-9/level-1.c"
echo "  figure-9/game.py"
echo "One may find the image of figure 9 (NOTE: this image is not generated, but provided as is) in:"
echo "  eog figure-9/game-display.png &"
echo
