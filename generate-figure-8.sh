#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"
[ -d easytracker/.venv ] || { echo "ERROR: easytracker/.venv directory does not exists" >&2; exit 1; }

source "$dir"/PARAMS.sh

source easytracker/.venv/bin/activate
export PYTHONPATH="$PWD/easytracker/extra-packages"

TOOLDIR=../paper-examples/recvis
OUTDIR=figure-8

rm -rf "$OUTDIR"
mkdir "$OUTDIR"
pushd "$OUTDIR" >/dev/null

# Disable display
DISPLAY= $TOOLDIR/recvis.py $TOOLDIR/mystery.py m x elems

popd >/dev/null

echo
echo "Graphs for figure 8 generated in figure-8/"
echo "One may open the figure corresponding to figure 8 with:"
echo "  eog figure-8/rec-013_src.jpg &"
echo "  eog figure-8/rec-013.svg &"
echo
